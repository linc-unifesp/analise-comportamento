#  Aula 3.2 - Manipulação de dados
install.packages("car")
install.packages("foreign")
library(foreign)
library(car)


# **** Carregar banco de dados
internet_df <- foreign::read.dta("../data sets/Dados_Internet_Macedo.dta")

# **** Manipulação de data frame
internet_df$Idade_Dias
internet_df[,"Genero"]

# **** Sumarios
View(internet_df)
summary(internet_df)
summary(internet_df$Genero)
summary(internet_df$Idade_Dias)

# Transformacao
internet_df$Idade_Anos <- internet_df$Idade_Dias/365

# **** Medidas de tendência central e dispersão
mean(internet_df$Idade_Anos)
median(internet_df$Idade_Anos)
sd(internet_df$Idade_Anos)
var(internet_df$Idade_Anos)

# Recode 
internet_df$Idade_Anos_rec <- car::recode(internet_df$Idade_Anos,
                                      "120:hi=NA;lo:0=NA")

# **** Salvar outputs
write.csv2(internet_df,"new_data-24-04.csv")

# **** Padrão de estilo e comentários
#  Reestruturar script