---
title: "Logistic Regression"
output: html_document
---

# Regress�o Log�stica

Assim como no modelo de Regress�o Linear, a **Regress�o Log�stica** � um m�todo estat�stico que procura obter predi��es a partir de uma ou mais vari�veis independentes. Entretanto, o modelo de predi��o que iremos obter n�o � mais uma equa��o linear. Isto ocorre pois nossa vari�vel dependente (predita) agora � categ�rica (frequentemente bin�ria). Portanto uma equa��o de achatamento � aplicada a equa��o linear, a fim de tratar nossas predi��es como probabilidade (0 ou 1). Esta fun��o aplicada � uma fun��o logistica (ou sigmoide) do tipo:

$\sigma(x)=1/(1+e^-x)$

```{r,echo=FALSE}
z=c(-20:20)
j=(1/(1+exp(1)^-z))
plot(z, j)
```

## Regress�o log�stica: Teste no R

Antes de nos atermos � visualiza��o dos dados, temos que verificar estat�sticamente a signific�ncia da nossa compara��o. Suponhamos que estamos tentando predizar se uma ferramenta � boa ou ruim a partir de um par�metro x qualquer (exemplo retirado da [linked phrase](http://blog.revolutionanalytics.com/2016/08/roc-curves-in-two-lines-of-code.html)). 

```{r,echo=FALSE}

simple_roc <- function(labels, scores){
  labels <- labels[order(scores, decreasing=TRUE)]
  data.frame(TPR=cumsum(labels)/sum(labels), FPR=cumsum(!labels)/sum(!labels), labels)
}
set.seed(1)
sim_widget_data <- function(N, noise=100){
  x <- runif(N, min=0, max=100)
  y <- 122 - x/2 + rnorm(N, sd=noise)
  bad_widget <- factor(y > 100)
  data.frame(x, y, bad_widget)
}
widget_data <- sim_widget_data(500, 10)

test_set_idx <- sample(1:nrow(widget_data), size=floor(nrow(widget_data)/4))

test_set <- widget_data[test_set_idx,]
training_set <- widget_data[-test_set_idx,]


```

```{r}
modelo <- glm(bad_widget ~ x, family = "binomial", data = widget_data)
summary(modelo)
```

Com estes resultados podemos fazer diversas interpreta��es. A primeira � de que de acordo com o $p-value$ encontrado, podemos concluir que o par�metro $x$ � um bom preditor para a qualidade da ferranta utilizada. Se quisermos quantificar esta predi��o em termos de raz�o de chances, podemos fazer a exponencial dos coeficientes do modelo de regress�o log�stica:

```{r}
exp(modelo$coefficients)
```

Com isto, chegamos � conclus�o que o aumento em $1$ unidade de $x$ aumenta em $7.8\%$ a chance da qualidade da ferramenta n�o ser ruim. 


##Visualiza��o Gr�fica da Regress�o Log�stica

###Limiar de Predi��o

Uma poss�vel abordagem a ser realizada ap�s a interpreta��o dos resultados do modelo de regress�o log�stica, � a tentativa de tra�ar um $x limiar$ para predizer a vari�vel de interesse. A visualiza��o dos dados se mostra fundamental para que tenhamos ideia de qual seria este limiar. Realizaremos, ent�o, um scatter plot entre os par�metros x (preditor) e y (vari�vel num�rica que gerou a dicot�mica de interesse: 'ferramentas ruins').

```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(dplyr)
library(ggplot2)
test_set %>% 
  ggplot(aes(x=x, y=y, col=bad_widget)) + 
  scale_color_manual(values=c("black", "red")) + 
  geom_point() + 
  ggtitle("Ferramentas ruins relacionadas a x")


```

Podemos perceber com o gr�fico acima que, com x abaixo de 20 todas as ferramentas s�o ruins, j� com x acima de 80, todas as ferramentas s�o boas. O limiar deve ent�o esatar entre estes 2 n�meros. 

Podemos tamb�m plotar nossos dados de regress�o log�stica de acordo com a dispers�o de nosso modelo. Isto �, podemos plotar ordenadamente nossas observa��es de x de acordo com a fun��o sigmoide descrita anteriormente ($fun��o\ predict$). Para isto, n�s temos de definir a varia��o de 0 � 100% da resposta com o intervalo de 0 � 1, e ser� o eixo y da nossa sigmoide:

```{r}

fit_glm <- glm(bad_widget ~ x, training_set, family=binomial(link="logit"))

glm_link_scores <- predict(fit_glm, test_set, type="link")

glm_response_scores <- predict(fit_glm, test_set, type="response")

score_data <- data.frame(link=glm_link_scores, 
                         resposta=glm_response_scores,
                         bad_widget=test_set$bad_widget,
                         stringsAsFactors=FALSE)

score_data %>% 
  ggplot(aes(x=link, y=resposta, col=bad_widget)) + 
  scale_color_manual(values=c("black", "red")) + 
  geom_point()
```


Com este plot, come�amos a perceber que o limiar de x que estamos buscando deve estar em torno de 0,7 da taxa de resposta. Isto pois o gr�fico evidencia este score dicotomiza a amostra de uma maneira porecida que nossa vari�vel dicot�mica de interesse. Esta taxa de resposta ser� utilizada para a constru��o da curva ROC, que ir� representar o poder de discernimento do modelo de regress�o log�stica com limiar de x.

###Curvas ROC e AUC

A curva ROC �, como dito anteriormente, uma curva gerada a partir da taxa de resposta do modelo de predi��o. Ela tem nos eixos as varia��es de sensibilidade (Verdadeiros positivos) e especificidade (falsos positivos). Estes eixos variam de 0 � 1, pois s�o quantificados de acordo com a taxa de resposta do modelo de predi��o log�stica. A curva ROC, assim como o �ltimo gr�fico plotado, � feita a partir da ordem de casos (True ou False) em rela��o ao par�metro $x$. A mensura��o da qualidade de discernimento do modelo � feita pelo c�lculo da �rea abaixo da curva ROC (AUC). Valores mais pr�imos a 1 demonstram um melhor modelo, j� os mais pr�ximos a 0.5 demonstram o contr�rio. 

```{r, message=FALSE, warning=FALSE}
library(pROC)
plot(roc(test_set$bad_widget, glm_response_scores, direction="<"),
     col="yellow", lwd=3, main="Ferramentas")
## 
## Call:
## roc.default(response = test_set$bad_widget, predictor = glm_response_scores,     direction = "<")
## 
## Data: glm_response_scores in 59 controls (test_set$bad_widget FALSE) < 66 cases (test_set$bad_widget TRUE).
## Area under the curve: 0.9037
glm_simple_roc <- simple_roc(test_set$bad_widget=="TRUE", glm_link_scores)
with(glm_simple_roc, points(1 - FPR, TPR, col=1 + labels))
```
```{r}

set.seed(1)
N <- 2000
P <- 0.01
rare_success <- sample(c(TRUE, FALSE), N, replace=TRUE, prob=c(P, 1-P))
guess_not <- rep(0, N)
plot(roc(rare_success, guess_not), print.auc=TRUE)
## 
## Call:
## roc.default(response = rare_success, predictor = guess_not)
## 
## Data: guess_not in 1978 controls (rare_success FALSE) < 22 cases (rare_success TRUE).
## Area under the curve: 0.5
simp_roc <- simple_roc(rare_success, guess_not)
with(simp_roc, lines(1 - FPR, TPR, col="blue", lty=2))
```
